**Teste**

```php

php bin/console server:start

bin/console generate:bundle --namespace=CIANDT/CommonBundle --bundle-name=CommonBundle --format=annotation --dir=src --shared --no-interaction

composer dump-autoload --optimize --no-dev --classmap-authoritative
composer install --optimize-autoloader --no-scripts

bin/console doctrine:schema:update --force
bin/console doctrine:database:create
bin/console hautelook_alice:doctrine:fixtures:load -n

docker exec -it cli bin/console hautelook_alice:doctrine:fixtures:load -n
sudo rm -rf var/cache/* var/logs/* var/sessions/*
mkdir -p var/cache var/logs var/sessions

sudo chmod -R 777 var/cache var/logs var/sessions
sudo setfacl -dR -m u::rwX var/cache var/logs var/sessions
```
