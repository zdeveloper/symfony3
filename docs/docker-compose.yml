version: '2'
networks:
  development:
    driver: bridge
    ipam:
      config:
      - subnet: 10.10.10.0/24

services:
    code:
        image: busybox:latest
        container_name: code
        volumes:
            - ${SYMFONY_APP_PATH}:/var/www/html
        command: /bin/echo ${SYMFONY_APP_NAME}
        tty: true
        networks:
          development:
        restart: always

    cli:
        image: diegograssato/cli
        container_name: cli
        hostname: cli
        tty: true
        volumes_from:
            - code
        volumes:
            - ~:/.home-linux   # Linux
        environment:
            CLI_SYMFONY_DEV: "true"
            CLI_CS: "true"
            CLI_SONAR: "true"
        networks:
          development:
        restart: always

    php:
        image: diegograssato/php7-alpine:v1.0
        container_name: php
        hostname: php
        volumes_from:
            - code
        links:
            - db
            - mailcatcher
        environment:
             PHP_XDEBUG_ENABLE: "on"
             PHP_XDEBUG_IDE_KEY: "PHPSTORM"
             PHP_XDEBUG_PROFILER_ENABLE: "off"
             PHP_XDEBUG_PROFILER_OUTPUT_DIR: "/tmp/profile"
             PHP_XDEBUG_PROFILER_OUTPUT_NAME: "dtux.out.%p"
             PHP_XDEBUG_REMOTE_LOG: "/tmp/php-dtux.log"
             PHP_XDEBUG_PORT: "9000"
             PHP_XDEBUG_REMOTE_IP: "10.10.10.1"
             PHP_XDEBUG_REMOTE_LOG_ENABLE: "on"
             PHP_PRODUCTION: "false"
             PHP_FPM_PORT: 5000
             PHP_OPCACHE_ENABLE: "true"
        networks:
          development:
            ipv4_address: 10.10.10.10
        restart: always

    nginx:
        image: diegograssato/nginx
        container_name: web
        hostname: web
        links:
            - php
            - db
            - mailcatcher
        ports:
            - 80:80
            - 443:443
        volumes_from:
            - code
        environment:
            PHP_FPM_SOCKET: "php:5000"
            NGINX_WORKER_PROCESSES: 8
            NGINX_CONNECTIONS: 40960 # 4 * 512 * 10
            NGINX_DOCUMENT_ROOT: "/var/www/html/"
            APP_INDEX_FILE: 'app_dev.php'
            VIRTUAL_HOST: "{'name':'sf3','domains':['sf3'],'docroot':'web', 'forcessl': 'false', 'model': 'symfony'}"
        networks:
          development:
            ipv4_address: 10.10.10.20
        restart: always

    mailcatcher:
        image: diegograssato/mailcatcher:v1
        container_name: ${SYMFONY__MAILER_HOST}
        hostname: ${SYMFONY__MAILER_HOST}
        expose:
          - ${SYMFONY__MAILER_PORT}
          - 1080
        ports:
          - 1080:1080
        environment:
          SMTP_PORT: ${SYMFONY__MAILER_PORT}
          HTTP_PORT: 1080
        networks:
          development:
        restart: always

    db:
        image: mariadb:5.5
        container_name: ${MYSQL_HOST}
        hostname: ${MYSQL_HOST}
        volumes:
          - ${SYMFONY_APP_PATH}/var/data/.${MYSQL_HOST}:/var/lib/mysql  # Mapping for custom mysql database folder.
        expose:
            - "3306"
        ports:
            - "3307:3306"
        environment:
            MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}
            MYSQL_DATABASE: ${MYSQL_DATABASE}
            MYSQL_USER: ${MYSQL_USER}
            MYSQL_PASSWORD: ${MYSQL_PASSWORD}
        networks:
          development:
            ipv4_address: 10.10.10.30
        restart: always

    phpmyadmin:
        container_name: ${MYSQL_HOST}_admin
        hostname: ${MYSQL_HOST}_admin
        image: phpmyadmin/phpmyadmin
        links:
            - db
        ports:
            - 9980:80
        environment:
            MYSQL_USERNAME: root
            MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}
            PMA_ARBITRARY: 1
            PMA_HOST: ${MYSQL_HOST}
            PMA_PORT: ${MYSQL_PORT}
            PMA_USER: root
            PMA_PASSWORD: ${MYSQL_ROOT_PASSWORD}
        networks:
          development:
        restart: always
