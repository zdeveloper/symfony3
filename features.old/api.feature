Background:
Given there are 7 "Muffin"s


Scenario: muffins.show Get a listing of muffins (w/ pagination and query string goodness)!
When I send a GET request to "/api/v2/en/index.json"
Then the response should contain json:
"""
    {
    "page": 2,
    "per_page": 5,
    "page_results": 2,
    "total_results": 7
    }
    """
And the response json's "items" key should be of type "array"
