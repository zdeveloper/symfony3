#!/usr/bin/env bash
php bin/console doctrine:schema:update --force
bin/console hautelook_alice:doctrine:fixtures:load -n
bin/console cache:clear --env=dev
bin/console debug:router
