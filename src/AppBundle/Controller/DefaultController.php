<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Pipeline\AddOneStage;
use AppBundle\Pipeline\TimesTwoStage;
use League\Pipeline\Pipeline;
use League\Pipeline\PipelineBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Zend\Diactoros\Response;


class DefaultController extends Controller
{
    /**
     * @Route("/{_locale}", defaults={"_locale": "en"}, requirements={
     *     "_locale": "en|fr|es"
     * })
     * @Template("AppBundle:default:index.html.twig")
     */
    public function indexAction(ServerRequestInterface $request)
    {

        $manager = $this->get('app.manager');
        $post = ['title' =>  'Test', 'content' =>'Test', 'slug' => "slu_test"];

        //$postObj = $manager->getObject($post);
        //$postObj->exchangeArray($post);
        //$manager->save($postObj);

        $repo = $manager->getRepo()->findAll();

//        $message = \Swift_Message::newInstance()
//            ->setSubject('Hello Email')
//            ->setFrom('send@example.com')
//            ->setTo('recipient@example.com')
//            ->setBody(
//                "Test"
//            )
//        ;
//        $this->get('mailer')->send($message);

        // Interact with the PSR-7 request
        return ['data' => $manager->main(),
            'posts' => $repo,
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ];
    }

    /**
     * @Route("/test")
     */
    public function test(ServerRequestInterface $request)
    {
        $response = new Response();
        $name = "Diego";
        $response->getBody()->write(sprintf("Hello %s", htmlspecialchars($name)));

        $pipeline = (new Pipeline)->pipe(function ($payload) {
            return $payload + 2;
        });

        // Returns 12
        dump($pipeline->process(10));

        $processApiRequest = (new Pipeline)
            ->pipe(new TimesTwoStage) // 2
            ->pipe(new AddOneStage); // 3

        $pipeline = (new Pipeline())
            ->pipe(new TimesTwoStage())
            ->pipe($processApiRequest)
            ->pipe(new AddOneStage());

        // Returns 16
        dump($pipeline->process(10));

        $pipelineBuilder = (new PipelineBuilder())
            ->add(new TimesTwoStage())
            ->add(new AddOneStage());

        // Build the pipeline
        $pipeline = $pipelineBuilder->build();

        // Returns 13
        dump($pipeline->process(10));

        return $response;
    }

    /**
     * @Route("/key")
     * @param ServerRequestInterface $request
     * @return JsonResponse
     */
    public function key(ServerRequestInterface $request)
    {
        $user = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findOneBy(['username' => 'diego']);
//
//        $user = new User('diego','diego');
//
//
//        $encode = $this->container->get("security.encoder_factory")
//            ->getEncoder($user);
//
//        $pass = $encode->encodePassword('diego', $user->getSalt());
//
//        $user->setRoles(['ROLE_ADMIN', 'ROLE_USER']);
//        $user->setPassword($pass);
//
//        $manager = $this->get('app.manager');
//        $manager->save($user);

        $token = $this->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $user->getUsername(),
                'exp' => time() + 3600 // 1 hour expiration
            ]);
         return new JsonResponse(['token' => $token]);
    }
}
