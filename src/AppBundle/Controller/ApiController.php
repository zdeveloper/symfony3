<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;

# https://codereviewvideos.com/course/symfony-3-with-reactjs-and-angular/video/simple-symfony-3-restful-api-setup
# https://github.com/willdurand/BazingaFakerBundle/blob/master/Resources/doc/index.md

class ApiController extends FOSRestController
{
    /**
     * Get single Page,
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "API",
     *   output = "AppBundle\Controller\Api",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Returned when the page is not found"
     *   }
     * )
     *
     * @Route("/api/v1/{_locale}/index.{_format}",
     *     defaults={
     *     "_format": "json",
     *     "_locale": "en"
     *      },
     *     requirements={
     *     "_format": "json|xml",
     *     "_locale": "en|fr|es"
     *     }
     *     )
     * @View()
     */
    public function getAction()
    {
        return ['a' => 10, 'b' => 20];
    }
}
