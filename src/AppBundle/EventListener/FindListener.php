<?php

namespace AppBundle\EventListener;

use AppBundle\Event\FindEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class FindListener
{
    public function onFind(FindEvent $event)
    {
        echo "<br>" . $event->getName() ."<br>";
        dump($event);
        var_dump($event);

    }
}
