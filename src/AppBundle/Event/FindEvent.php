<?php

namespace AppBundle\Event;
use Symfony\Component\EventDispatcher\Event;

class FindEvent extends Event
{
    const NAME = 'order.placed';
    public $name;
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }



}
