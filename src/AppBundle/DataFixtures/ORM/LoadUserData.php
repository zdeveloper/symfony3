<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 17/04/16
 * Time: 04:27
 */

namespace AppBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    private $container;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setUsername("admin");
        $admin->setEmail("admin@gmail.com");
        $admin->setEnabled(true);
        $admin->setPassword($this->encodePassword($admin, 'admin'));

        $alice = new User();
        $alice->setUsername("Alice");
        $alice->setEmail("alice@gmail.com");
        $alice->setPassword($this->encodePassword($alice, 'user'));

        $bob = new User();
        $bob->setUsername("Bob");
        $bob->setEmail("bob@gmail.com");
        $bob->setPassword($this->encodePassword($bob, 'user'));

        $manager->persist($admin);
        $manager->persist($bob);
        $manager->persist($alice);
        $manager->flush();
    }

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    private function encodePassword($user, $plainPassword)
    {
        $encode = $this->container->get("security.encoder_factory")
            ->getEncoder($user);

        return $encode->encodePassword($plainPassword, $user->getSalt());
    }
}
