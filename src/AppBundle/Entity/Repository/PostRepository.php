<?php

namespace AppBundle\Entity\Repository;

class PostRepository extends AbstractRepository
{
    public function queryLatest()
    {
        return $this->getEntityManager()
            ->createQuery('
                SELECT p
                FROM AppBundle:Post p
                WHERE p.publishedAt <= :now
                ORDER BY p.publishedAt DESC
            ')
            ->setParameter('now', new \DateTime())
        ;
    }

    public function findLatest()
    {
        $this->queryLatest()->getResult();
    }
}
