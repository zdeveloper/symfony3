<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 17/01/17
 * Time: 09:40
 */

namespace AppBundle\Pipeline;

use League\Pipeline\StageInterface;

class AddOneStage implements StageInterface
{
    public function __invoke($payload)
    {
        return $payload + 1;
    }
}
